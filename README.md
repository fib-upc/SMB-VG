To execute in GNU/Linux:

- Install the required libraries (see Makefile)
- Modify the header of Textures.cpp from `<SOIL.h>` to `<SOIL/SOIL.h>`
